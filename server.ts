import {
    Application,
    Context
} from "https://deno.land/x/oak@v6.3.2/mod.ts";
import { WebSocketMiddleware, handler } from "https://raw.githubusercontent.com/jcc10/oak_websoket_middleware/v1.0.1/mod.ts";
import { WebSocket, isWebSocketCloseEvent, isWebSocketPingEvent } from 'https://deno.land/std@0.77.0/ws/mod.ts'

const socketHandler: handler = async function (socket: WebSocket, url: URL): Promise<void> {
    // Wait for new messages
    try {
        for await (const ev of socket) {
            if (ev instanceof Uint8Array) {
                // binary message
                const id = new Uint8Array(ev, 0, 1)[0];
                const slice = Uint8Array.BYTES_PER_ELEMENT * 1
                const noHeader = ev.buffer.slice(slice);
                const text = await new Blob([noHeader]).text();
                console.log(`[${id}] ${text}`);
                socket.send(ev);
            } else if (isWebSocketPingEvent(ev)) {
                const [, body] = ev;
                // ping
                // stub
                //console.log("ws:Ping", body);
            } else if (isWebSocketCloseEvent(ev)) {
                // close
                const { code, reason } = ev;
                //console.log("ws:Close", code, reason);
            }
        }
    } catch (err) {
        console.error(`failed to receive frame: ${err}`);

        if (!socket.isClosed) {
            await socket.close(99).catch(console.error);
        }
    }
}

const app = new Application();
app.use(WebSocketMiddleware(socketHandler));
app.use(async (ctx: Context) => {
    const decoder = new TextDecoder("utf-8");
    const bytes = Deno.readFileSync("./client.html");
    const text = decoder.decode(bytes);
    ctx.response.body = text;
});
const appPromise = app.listen({ port: 3000 });
console.log("Server running on localhost:3000");